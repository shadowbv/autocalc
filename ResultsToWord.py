from docx import Document
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.enum.table import WD_TABLE_ALIGNMENT, WD_ALIGN_VERTICAL
from docx.enum.section import WD_ORIENT
from docx.enum.section import WD_SECTION
from docx.enum.text import WD_BREAK
import numpy as np
from docx.shared import Inches, Pt, Cm
import os


class WriteResultsToWord:
    def __init__(self):
        super(WriteResultsToWord, self).__init__()
        self.table_title = "Залишкове енерговиділення, Вт/тU"
        self.table_period = "Витрим-ка, років"

    def change_period(self):
        self.table_period = "Витрим-ка, діб"

    def write_heat_result_to_docx(self, filename, result_filename):
        if os.path.isfile(filename + '.docx'):
            document = Document(filename + '.docx')
        else:
            document = Document()
            current_section = document.sections[-1]
            new_width, new_height = current_section.page_height, current_section.page_width
            new_section = document.add_section(WD_SECTION.NEW_PAGE)
            new_section.orientation = WD_ORIENT.LANDSCAPE
            new_section.page_width = new_width
            new_section.page_height = new_height

        style = document.styles['Normal']
        font = style.font
        font.name = 'Times New Roman'
        font.size = Pt(11)
        # document.add_paragraph()

        margin = 2
        sections = document.sections
        for section in sections:
            section.top_margin = Cm(margin - 0.6)
            section.bottom_margin = Cm(margin - 0.6)
            section.left_margin = Cm(margin)
            section.right_margin = Cm(margin - 0.5)
        # ----------------------------------------------------------------------------------
        data = self.data_to_matrix(result_filename)

        table = document.add_table(data.shape[0] + 2, data.shape[1])

        # table.alignment = WD_TABLE_ALIGNMENT.CENTER

        table.style = 'TableGrid'
        table.alignment = WD_TABLE_ALIGNMENT.CENTER
        # table.autofit = True

        a = table.cell(0, 0)
        b = table.cell(2, 0)
        A = a.merge(b)

        a = table.cell(0, 1)
        b = table.cell(0, 12)
        A = a.merge(b)

        a = table.cell(1, 1)
        b = table.cell(1, 12)
        A = a.merge(b)

        table.cell(0, 0).text = self.table_period
        table.cell(0, 0).vertical_alignment = WD_ALIGN_VERTICAL.CENTER
        table.cell(0, 0).paragraphs[0].paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER

        table.cell(0, 1).text = self.table_title
        table.cell(0, 1).vertical_alignment = WD_ALIGN_VERTICAL.CENTER
        table.cell(0, 1).paragraphs[0].paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER

        table.cell(1, 1).text = "Вигоряння ПК, МВт*діб/кгU"
        table.cell(1, 1).vertical_alignment = WD_ALIGN_VERTICAL.CENTER
        table.cell(1, 1).paragraphs[0].paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER

        # ----------------- write bups----------------
        for num, bup in enumerate(data[0][1:]):
            table.cell(2, num + 1).text = "{}".format(bup)
            table.cell(2, num + 1).vertical_alignment = WD_ALIGN_VERTICAL.CENTER
            table.cell(2, num + 1).paragraphs[0].paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER

        for row in range(data.shape[0] - 1):
            for col in range(data.shape[1]):
                # print('{:.3e}'.format(data[1 + row][col]), end='')
                if col == 0:
                    table.cell(3 + row, col).text = "{:.1f}".format(data[1 + row][col])
                else:
                    table.cell(3 + row, col).text = "{:.3E}".format(data[1 + row][col])
                table.cell(3 + row, col).vertical_alignment = WD_ALIGN_VERTICAL.CENTER
                table.cell(3 + row, col).paragraphs[0].paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER
            # print(end='\n')

        paragraph = document.add_paragraph()
        run = paragraph.add_run()
        run.add_break(WD_BREAK.PAGE)
        # document.add_paragraph()

        document.save(filename + '.docx')

    def write_neutron_result_to_docx(self, bup, filename, result_filename):
        if os.path.isfile(filename + '.docx'):
            document = Document(filename + '.docx')
        else:
            document = Document()
            current_section = document.sections[-1]
            new_width, new_height = current_section.page_height, current_section.page_width
            new_section = document.add_section(WD_SECTION.NEW_PAGE)
            new_section.orientation = WD_ORIENT.LANDSCAPE
            new_section.page_width = new_width
            new_section.page_height = new_height

        style = document.styles['Normal']
        font = style.font
        font.name = 'Times New Roman'
        font.size = Pt(11)
        # document.add_paragraph()

        margin = 2
        sections = document.sections
        for section in sections:
            section.top_margin = Cm(margin - 0.6)
            section.bottom_margin = Cm(margin - 0.6)
            section.left_margin = Cm(margin)
            section.right_margin = Cm(margin - 0.5)
        # ----------------------------------------------------------------------------------
        data = self.data_to_matrix(result_filename)
        # print(data.shape[1])

        table = document.add_table(data.shape[0] + 2, data.shape[1])

        table.style = 'TableGrid'
        table.alignment = WD_TABLE_ALIGNMENT.CENTER
        # table.autofit = True

        a = table.cell(0, 0)
        b = table.cell(0, data.shape[1] - 1)
        A = a.merge(b)

        a = table.cell(1, 0)
        b = table.cell(2, 0)
        A = a.merge(b)

        a = table.cell(1, 1)
        b = table.cell(1, data.shape[1] - 1)
        A = a.merge(b)

        #
        table.cell(0, 0).text = f"Вигорання {bup:.0f} МВт*діб/кг U"
        table.cell(0, 0).vertical_alignment = WD_ALIGN_VERTICAL.CENTER
        table.cell(0, 0).paragraphs[0].paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER
        #
        table.cell(1, 0).text = "№ \n гр."
        table.cell(1, 0).vertical_alignment = WD_ALIGN_VERTICAL.CENTER
        table.cell(1, 0).paragraphs[0].paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER
        #
        table.cell(1, 1).text = "Витримка, років"
        table.cell(1, 1).vertical_alignment = WD_ALIGN_VERTICAL.CENTER
        table.cell(1, 1).paragraphs[0].paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER
        #
        # ----------------- write delays----------------
        for num, delay in enumerate(data[0][1:]):
            table.cell(2, num + 1).text = "{:.0f}".format(delay)
            table.cell(2, num + 1).vertical_alignment = WD_ALIGN_VERTICAL.CENTER
            table.cell(2, num + 1).paragraphs[0].paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER
        #
        for row in range(data.shape[0] - 1):
            for col in range(data.shape[1]):
                # print('{:.3e}'.format(data[1 + row][col]), end='')
                if col == 0:
                    table.cell(3 + row, col).text = "{:.0f}".format(data[1 + row][col])
                else:
                    table.cell(3 + row, col).text = "{:.2E}".format(data[1 + row][col])
                table.cell(3 + row, col).vertical_alignment = WD_ALIGN_VERTICAL.CENTER
                table.cell(3 + row, col).paragraphs[0].paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER
            # print(end='\n')

        # paragraph = document.add_paragraph()
        # run = paragraph.add_run()
        # run.add_break(WD_BREAK.PAGE)
        # document.add_paragraph()

        document.save(filename + '.docx')

    def write_nuclide_result_to_docx(self, filename, result_filename):
        if os.path.isfile(filename + '.docx'):
            document = Document(filename + '.docx')
        else:
            document = Document()
            current_section = document.sections[-1]
            new_width, new_height = current_section.page_height, current_section.page_width
            new_section = document.add_section(WD_SECTION.NEW_PAGE)
            new_section.orientation = WD_ORIENT.LANDSCAPE
            new_section.page_width = new_width
            new_section.page_height = new_height

        style = document.styles['Normal']
        font = style.font
        font.name = 'Times New Roman'
        font.size = Pt(11)
        # document.add_paragraph()

        margin = 2
        sections = document.sections
        for section in sections:
            section.top_margin = Cm(margin - 0.6)
            section.bottom_margin = Cm(margin - 0.6)
            section.left_margin = Cm(margin)
            section.right_margin = Cm(margin - 0.5)
        # ----------------------------------------------------------------------------------
        data = self.nucl_data_to_matrix(result_filename)
        # print(data.shape[1])

        table = document.add_table(data.shape[0] + 2, data.shape[1])

        table.style = 'TableGrid'
        table.alignment = WD_TABLE_ALIGNMENT.CENTER
        # table.autofit = True

        a = table.cell(0, 0)
        b = table.cell(2, 0)
        A = a.merge(b)

        a = table.cell(0, 1)
        b = table.cell(0, data.shape[1] - 1)
        A = a.merge(b)

        a = table.cell(1, 1)
        b = table.cell(1, data.shape[1] - 1)
        A = a.merge(b)

        table.cell(0, 0).text = "Ізотоп"
        table.cell(0, 0).vertical_alignment = WD_ALIGN_VERTICAL.CENTER
        table.cell(0, 0).paragraphs[0].paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER

        table.cell(0, 1).text = "Ізотопний склад опроміненого палива г/тU"
        table.cell(0, 1).vertical_alignment = WD_ALIGN_VERTICAL.CENTER
        table.cell(0, 1).paragraphs[0].paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER

        table.cell(1, 1).text = "Вигоряння ПК, МВт*діб/кгU"
        table.cell(1, 1).vertical_alignment = WD_ALIGN_VERTICAL.CENTER
        table.cell(1, 1).paragraphs[0].paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER

        # ----------------- write bups----------------
        for num, bup in enumerate(data[0][1:]):
            table.cell(2, num + 1).text = "{}".format(bup)
            table.cell(2, num + 1).vertical_alignment = WD_ALIGN_VERTICAL.CENTER
            table.cell(2, num + 1).paragraphs[0].paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER
        #
        for row in range(data.shape[0] - 1):
            for col in range(data.shape[1]):
                # print('{:.3e}'.format(data[1 + row][col]), end='')
                if col == 0:
                    table.cell(3 + row, col).text = "{}".format(data[1 + row][col])
                else:
                    table.cell(3 + row, col).text = "{}".format(data[1 + row][col])
                table.cell(3 + row, col).vertical_alignment = WD_ALIGN_VERTICAL.CENTER
                table.cell(3 + row, col).paragraphs[0].paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER
        #     # print(end='\n')

        paragraph = document.add_paragraph()
        run = paragraph.add_run()
        run.add_break(WD_BREAK.PAGE)
        # document.add_paragraph()

        document.save(filename + '.docx')

    @staticmethod
    def data_to_matrix(filename: str):
        matrix = []
        with open(filename, 'r') as file:
            for line in file:
                matrix.append([float(x) for x in line.strip().split()])
                # print(line)
            # print(matrix[1])
            # print([x for x in line1.strip()])
            # y_values1 = [float(x) for x in line2.strip().split()]
            # print(y_values1)
            matrix = np.array(matrix)
            # for row in range(matrix.shape[1]):
            #     for col in range(matrix.shape[0]):
            #         file.write("{:.3e} ".format(matrix[col][row]))
            #     file.write("\n")
        return matrix

    @staticmethod
    def nucl_data_to_matrix(filename: str):
        matrix = []
        with open(filename, 'r') as file:
            for line in file:
                matrix.append([x for x in line.strip().split()])
            matrix = np.array(matrix)
        return matrix


class WriteResultsToWordActivity(WriteResultsToWord):
    def __init__(self):
        super(WriteResultsToWord, self).__init__()
        self.table_title = "Активність, Бк/тU"
        self.table_period = "Витрим-ка, років"


if __name__ == "__main__":
    pass
    # handler = WriteResultsToWordActivity()
    # handler.change_period()
    # handler.write_heat_result_to_docx('results', 'years_activ-y.dat')
    # WriteResultsToWord().write_neutron_result_to_docx(5.0, 'results_neut', 'years_neutron_Bup_0-y.dat')
    # write_result_to_docx('heat-d', 'days-d.dat')
    # print(data_to_matrix('res-y.dat')[0][1:])
    # WriteResultsToWord().write_nuclide_result_to_docx('nuclide', 'nuclide_data-y.dat')
