from write_template import render_origens_template
from residual_handler import get_file_size, write_header_res_to_file, write_res_to_file
import os


def process_activity(bup, output_file_name, delays, time_type):
    origen_template_name = 'origens_activity'
    if len(delays) <= 20:
        saved_name = output_file_name
        render_origens_template(origen_template_name, saved_name, delays, len(delays), time_type)
        os.system("scale5 {}.inp".format(saved_name))
        if not get_file_size(saved_name + '.dat'):
            write_header_res_to_file((saved_name + '._plot000.plt', ), saved_name + '.dat')
        write_res_to_file(bup, (saved_name + '._plot000.plt', ), saved_name + '.dat')
    else:
        saved_name = output_file_name
        saved_name_1 = output_file_name + "_1"
        saved_name_2 = output_file_name + "_2"

        render_origens_template(origen_template_name, saved_name_1, delays[:20], len(delays[:20]), time_type)
        render_origens_template(origen_template_name, saved_name_2, delays[20:], len(delays[20:]), time_type)
        os.system("scale5 {}.inp".format(saved_name_1))
        os.system("scale5 {}.inp".format(saved_name_2))

        if not get_file_size(saved_name + '.dat'):
            write_header_res_to_file((saved_name_1 + '._plot000.plt', saved_name_2 + '._plot000.plt'),
                                     saved_name + '.dat')
        write_res_to_file(bup, (saved_name_1 + '._plot000.plt', saved_name_2 + '._plot000.plt'), saved_name + '.dat')


if __name__ == "__main__":
    pass
    # print(ret_data_from_plot('years._plot000.plt'))
    # print(ret_header_from_plot(('years._plot000.plt',)))
    # write_header_res_to_file('years._plot000.plt', 'results-d.dat')
    # write_res_to_file(20.0, ('days_2._plot000.plt', 'days_2._plot000.plt'))
    # flip_rez("results-d.dat", 0)