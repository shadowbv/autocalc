from write_template import render_origens_template
from opus import flip_rez
from ResultsToWord import WriteResultsToWord
import os


def get_file_size(filename: str) -> bool:
    try:
        b = os.path.getsize(filename)
        if b:
            return True
        else:
            return False
    except:
        pass


def process_gamma(bup, output_file_name, delays, time_type):
    origen_template_name = 'origens_gamma'
    for delay in delays:
        saved_name = output_file_name
        render_origens_template(origen_template_name, saved_name, delay, 1, time_type)
        os.system("scale5 {}.inp".format(saved_name))
        if not get_file_size(saved_name + '_Bup' + f'_{bup}' + '.dat'):
            write_header_to_res_file(saved_name + '._plot000.plt', saved_name + '_Bup' + f'_{bup}' + '.dat')
        write_res_to_file(delay, saved_name + '._plot000.plt', saved_name + '_Bup' + f'_{bup}' + '.dat')
    flip_rez(saved_name + '_Bup' + f'_{bup}' + '.dat', saved_name + '_Bup' + f'_{bup}', 0)
    WriteResultsToWord().write_neutron_result_to_docx(bup, 'results_gamma', saved_name + '_Bup' + f'_{bup:.0f}-y.dat')


def ret_header_from_plot(filename: str) -> list:
    data = []
    with open(filename, 'r') as file:
        for _ in range(6):
            file.readline()
        num = 1
        for line in file:
            if num % 2 == 0:
                values = [float(x) for x in line.strip().split()]
                data.append(values[1])
            num += 1
    return data


def write_header_to_res_file(filename: str, res_filename: str):
    data = ret_header_from_plot(filename)
    tmp = None
    with open(res_filename, 'a') as file:
        file.write("{:.2f}".format(0.0))
        for val in range(len(data)):
            if val != tmp:
                file.write("  {:8}".format(val + 1))
            else:
                pass
            tmp = val
        file.write("\n")


def ret_data_from_plot(filename: tuple) -> list:
    data = []
    with open(filename, 'r') as file:
        for _ in range(6):
            file.readline()
        num = 1
        for line in file:
            if num % 2 == 0:
                values = [float(x) for x in line.strip().split()]
                data.append(values[1])
            num += 1
    return data


def write_res_to_file(bup, filename: tuple, res_filename: str):
    data = ret_data_from_plot(filename)
    tmp = None
    with open(res_filename, 'a') as file:
        file.write("{:4} ".format(bup))
        for val in data:
            if val != tmp:
                file.write("{:.3e} ".format(val))
            else:
                pass
            tmp = val
        file.write("\n")


if __name__ == "__main__":
    days_type = 4  # days
    year_type = 5  # year
    year_delays = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 15, 20, 25, 30, 40, 50, 55]
    burnup = 60
    step = 5
    bups = [x + 5 for x in range(0, burnup, step)]
    # bups = [5, 10]
    for bup in bups:
        process_gamma(bup, 'years_gamma', year_delays[3:], year_type)
    # print(ret_header_from_plot('years_neutron._plot000.plt'))
    # write_header_to_res_file('years_neutron._plot000.plt', 'years_neutron_0.0.dat')
    # write_res_to_file(2, 'years_neutron._plot000.plt', 'years_neutron_0.0.dat')
    # process_activity(bup, output_name_days + '_activ', day_delays, days_type)
