from write_template import render_sas2h_template
from residual_handler import process_residual_heat
from activity_handler import process_activity
from neutrons_handler import process_neutrons
from gamma_handler import process_gamma
from nuclide_handler import process_nuclide, flip_nuclide_rez
from opus import write_rez, flip_rez
from ResultsToWord import WriteResultsToWord, WriteResultsToWordActivity
import os
import time

if __name__ == "__main__":
    start = time.time()
    burnup = 60
    step = 5
    bups = [x + 5 for x in range(0, burnup, step)]
    print(bups)
    # bups = [5, 10, 15, 20, 30, 35, 40, 45, 56]
    # --------------------------------------------- sensitives data ----------------------------------------------
    fuel_temp = 800  # K
    mod_temp = 558  # K
    power = 31.00  # Wt/TU, WWER-440
    step_for_bup = 5  # MWt*day/kg U
    # bup_step = 33.333333
    # fuelenght = 637.345
    # fuelenght = 1910.0
    fuelenght = 1960.0  # WWER-440
    # ------------------------------------------------------------------------------------------------------------
    # bups = [5, 10]
    day_delays = [0, 1, 2, 3, 4, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 60, 70, 80, 90, 100, 120, 140, 160, 180, 200,
                  220, 240, 260, 280, 300, 320, 340, 365]
    year_delays = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 15, 20, 25, 30, 40, 50, 55]
    days_type = 4  # days
    year_type = 5  # year
    # ------------------------------------------------------------------------------------------------------------
    sas2h_template_name = "438-440PK"  # "SOU"

    output_name_year = "years"
    output_name_days = "days"
    # filey_h = open(output_name_year + "_heat" + ".dat", 'w')
    # filed_h = open(output_name_days + "_heat" + ".dat", 'w')
    # filey_a = open(output_name_year + "_activ" + ".dat", 'w')
    # filed_a = open(output_name_days + "_activ" + ".dat", 'w')
    # filey.close()
    # filed.close()
    os.system('del *.dat')
    os.system('del *.docx')
    # ------------------------------------------------------------------------------------------------------------

    for bup in bups:
        # -----------------------------------sas2h----------------------------------------------------
        render_sas2h_template(bup, sas2h_template_name, power, step_for_bup, fuelenght, fuel_temp, mod_temp)
        os.system("scale5 {}.inp".format(sas2h_template_name))
        # -----------------------------------origens and opus for residual heat-----------------------
        process_residual_heat(bup, output_name_year + '_heat', year_delays, year_type)
        process_residual_heat(bup, output_name_days + '_heat', day_delays, days_type)
        # -----------------------------------origens for neutron spectrum-----------------------------
        process_neutrons(bup, output_name_year + '_neutron', year_delays[3:], year_type)
        # -----------------------------------origens for gamma spectrum-------------------------------
        process_gamma(bup, output_name_year + '_gamma', year_delays[3:], year_type)
        # -----------------------------------origens and opus for activity----------------------------
        process_activity(bup, output_name_year + '_activ', year_delays, year_type)
        process_activity(bup, output_name_days + '_activ', day_delays, days_type)
        # -----------------------------------origens for isotope composition--------------------------
        process_nuclide(bup, 'nuclide', year_delays, year_type)
        # --------------------------------------------------------------------------------------------
    # -----------------------------------residual heat--------------------------------------------
    results_file_name = 'Heat_and_activity'  # One word document instead of one file for each table
    flip_rez(output_name_year + '_heat' + ".dat", output_name_year + '_heat', 0)
    WriteResultsToWord().write_heat_result_to_docx(results_file_name, output_name_year + '_heat' + '-y.dat')

    flip_rez(output_name_days + '_heat' + ".dat", output_name_days + '_heat', 1)
    handler = WriteResultsToWord()
    handler.change_period()
    handler.write_heat_result_to_docx(results_file_name, output_name_days + '_heat' + '-d.dat')
    # -----------------------------------activity--------------------------------------------------
    flip_rez(output_name_year + '_activ' + ".dat", output_name_year + '_activ', 0)
    WriteResultsToWordActivity().write_heat_result_to_docx(results_file_name, output_name_year + '_activ' + '-y.dat')

    flip_rez(output_name_days + '_activ' + ".dat", output_name_days + '_activ', 1)
    handler = WriteResultsToWordActivity()
    handler.change_period()
    handler.write_heat_result_to_docx(results_file_name, output_name_days + '_activ' + '-d.dat')
    # ---------------------------------Nuclide concentration---------------------------------------
    flip_nuclide_rez('nuclide_data.dat', 'nuclide_data', 0)
    WriteResultsToWord().write_nuclide_result_to_docx('nuclide', 'nuclide_data-y.dat')
    # ---------------------------------------------------------------------------------------------
    done = time.time()
    print("{} - min".format((done - start) / 60))
