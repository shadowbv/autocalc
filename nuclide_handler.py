from write_template import render_origens_template
import numpy as np
from ResultsToWord import WriteResultsToWord
import os


def get_file_size(filename: str) -> bool:
    try:
        b = os.path.getsize(filename)
        if b:
            return True
        else:
            return False
    except:
        pass


def process_nuclide(bup, output_file_name, delays, time_type):
    origen_template_name = 'origens_nuclide'
    # for delay in delays:
    saved_name = output_file_name
    render_origens_template(origen_template_name, saved_name, delays, 1, time_type)
    os.system("scale5 {}.inp".format(saved_name))
    if not get_file_size(saved_name + '_data' + '.dat'):
        write_header_to_res_file(saved_name + '._plot000.plt', saved_name + '_data' + '.dat')
    write_res_to_file(bup, saved_name + '._plot000.plt', saved_name + '_data' + '.dat')
    # flip_rez(saved_name + '_Bup' + f'_{bup}' + '.dat', saved_name + '_Bup' + f'_{bup}', 0)
    # WriteResultsToWord().write_neutron_result_to_docx(bup, 'results_gamma', saved_name + '_Bup' + f'_{bup:.0f}-y.dat')


def ret_header_from_plot(filename: str) -> list:
    data = []
    with open(filename, 'r') as file:
        for _ in range(6):
            file.readline()
        for line in file:
            values = [x for x in line.strip().split()]
            if values[0] == 'total' or 'zr' in values[0]:
                pass
            else:
                data.append(values[0])
    return data


def write_header_to_res_file(filename: str, res_filename: str):
    data = ret_header_from_plot(filename)
    tmp = None
    with open(res_filename, 'a') as file:
        file.write("{:.2f}".format(0.0))
        for val in data:
            if val != tmp:
                file.write("{:>10}".format(val))
            else:
                pass
            tmp = val
        file.write("\n")


def ret_data_from_plot(filename: tuple) -> list:
    data = []
    with open(filename, 'r') as file:
        for _ in range(6):
            file.readline()
        for line in file:
            values = [x for x in line.strip().split()]
            if values[0] == 'total' or 'zr' in values[0]:
                pass
            else:
                data.append(float(values[1]))
    return data


def write_res_to_file(bup, filename: tuple, res_filename: str):
    data = ret_data_from_plot(filename)
    tmp = None
    with open(res_filename, 'a') as file:
        file.write("{:4} ".format(bup))
        for val in data:
            if val != tmp:
                file.write("{:.3e} ".format(val))
            else:
                pass
            tmp = val
        file.write("\n")


def flip_nuclide_rez(filename: str, result_filename, ind: int):
    matrix = []
    with open(filename, 'r') as file:
        for line in file:
            matrix.append([x for x in line.strip().split()])
            # print(line)
        # print(matrix[1])
        # print([x for x in line1.strip()])
        # y_values1 = [float(x) for x in line2.strip().split()]
        # print(y_values1)
        matrix = np.array(matrix)
        # print(matrix.shape[1])
        if ind == 0:
            file = open(result_filename + "-y.dat", 'w')
        else:
            file = open(result_filename + "-d.dat", 'w')
        for row in range(matrix.shape[1]):
            for col in range(matrix.shape[0]):
                file.write("{:8} ".format(matrix[col][row]))
            file.write("\n")


if __name__ == "__main__":
    days_type = 4  # days
    year_type = 5  # year
    year_delays = [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 15, 20, 25, 30, 40, 50, 55]
    bups = [5, 10]
    # process_nuclide(bups[0], 'nuclide', year_delays, year_type)
    # print(ret_header_from_plot('years_neutron._plot000.plt'))
    # write_header_to_res_file('data_nuclides._plot000.plt', 'data_nuclide.dat')
    # write_res_to_file(60, 'data_nuclides._plot000.plt', 'data_nuclide.dat')
    # process_activity(bup, output_name_days + '_activ', day_delays, days_type)
    flip_nuclide_rez('nuclide_data.dat', 'nuclide_data', 0)
