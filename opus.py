import csv
import numpy as np


def ret_str(filename1: str, filename2: str):
    with open(filename1, 'r') as file:
        for _ in range(6):
            file.readline()
        line1 = file.readline()
        line2 = file.readline()
        # print([x for x in line1.strip()])
        y_values1 = [float(x) for x in line2.strip().split()]
        # print(y_values1)
    with open(filename2, 'r') as file:
        for _ in range(6):
            file.readline()
        line3 = file.readline()
        line4 = file.readline()
    y_values2 = [float(x) for x in line4.strip().split()]
    # for y in y_values2:
    #     y_values1.append(y)
    return y_values1 + y_values2


def write_rez(filename: str):
    days_rez = ret_str('{}._plot000.plt'.format(filename), '{}._plot001.plt'.format(filename))
    tmp = None
    with open("results-d.dat", 'a') as file:
        for val in days_rez:
            if val != tmp:
                file.write("{:.3e} ".format(val))
            else:
                pass
            tmp = val
        file.write("\n")

    years_rez = ret_str('{}._plot002.plt'.format(filename), '{}._plot003.plt'.format(filename))
    tmp = None
    with open("results-y.dat", 'a') as file:
        for val in years_rez:
            if val != tmp:
                file.write("{:.3e} ".format(val))
            else:
                pass
            tmp = val
        file.write("\n")


def flip_rez(filename: str, result_filename, ind: int):
    matrix = []
    with open(filename, 'r') as file:
        for line in file:
            matrix.append([float(x) for x in line.strip().split()])
            # print(line)
        # print(matrix[1])
        # print([x for x in line1.strip()])
        # y_values1 = [float(x) for x in line2.strip().split()]
        # print(y_values1)
        matrix = np.array(matrix)
        # print(matrix.shape[1])
        if ind == 0:
            file = open(result_filename + "-y.dat", 'w')
        else:
            file = open(result_filename + "-d.dat", 'w')
        for row in range(matrix.shape[1]):
            for col in range(matrix.shape[0]):
                file.write("{:.3e} ".format(matrix[col][row]))
            file.write("\n")


if __name__ == "__main__":
    # print(ret_data_from_plot('years._plot000.plt'))
    # print(ret_header_from_plot('years._plot000.plt'))
    # write_header_res_to_file('years._plot000.plt', 'results-d.dat')
    # write_res_to_file('years._plot000.plt', 'results-d.dat')
    flip_rez("results-d.dat", 0)
    # flip_rez("results-d.dat", 1)
    # days_rez = ret_str('outputg._plot000.plt', 'outputg._plot001.plt')
    # years_rez = ret_str('outputg._plot002.plt', 'outputg._plot003.plt')
    # print(years_rez)
    # tmp = None
    # with open("rez-y.txt", 'w') as file:
    #     for val in years_rez:
    #         if val != tmp:
    #             file.write("{:.3e}\n".format(val))
    #         else:
    #             pass
    #         tmp = val

    # tmp = None
    # with open("rez-d.txt", 'w') as file:
    #     for val in days_rez:
    #         if val != tmp:
    #             file.write("{:.3e}\n".format(val))
    #         else:
    #             pass
    #         tmp = val
