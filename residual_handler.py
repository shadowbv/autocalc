from write_template import render_origens_template
import os


def get_file_size(filename: str) -> bool:
    try:
        b = os.path.getsize(filename)
        if b:
            return True
        else:
            return False
    except:
        pass


def process_residual_heat(bup, output_file_name, delays, time_type):
    origen_template_name = 'origens_residual'
    if len(delays) <= 20:
        saved_name = output_file_name
        render_origens_template(origen_template_name, saved_name, delays, len(delays), time_type)
        os.system("scale5 {}.inp".format(saved_name))
        if not get_file_size(saved_name + '.dat'):
            write_header_res_to_file((saved_name + '._plot000.plt', ), saved_name + '.dat')
        write_res_to_file(bup, (saved_name + '._plot000.plt', ), saved_name + '.dat')
    else:
        saved_name = output_file_name
        saved_name_1 = output_file_name + "_1"
        saved_name_2 = output_file_name + "_2"

        render_origens_template(origen_template_name, saved_name_1, delays[:20], len(delays[:20]), time_type)
        render_origens_template(origen_template_name, saved_name_2, delays[20:], len(delays[20:]), time_type)
        os.system("scale5 {}.inp".format(saved_name_1))
        os.system("scale5 {}.inp".format(saved_name_2))

        if not get_file_size(saved_name + '.dat'):
            write_header_res_to_file((saved_name_1 + '._plot000.plt', saved_name_2 + '._plot000.plt'),
                                     saved_name + '.dat')
        write_res_to_file(bup, (saved_name_1 + '._plot000.plt', saved_name_2 + '._plot000.plt'), saved_name + '.dat')


def ret_data_from_plot(filename: tuple) -> list:
    data = []
    for filename_one in filename:
        with open(filename_one, 'r') as file:
            for _ in range(6):
                file.readline()
            line1 = file.readline()
            line2 = file.readline()
            # print([x for x in line1.strip()])
            values = [float(x) for x in line2.strip().split()]
        data += values
    return data


def ret_header_from_plot(filename: tuple) -> list:
    data = []
    for filename_one in filename:
        with open(filename_one, 'r') as file:
            for _ in range(6):
                file.readline()
            line1 = file.readline()
            # line2 = file.readline()
            # print([x for x in line1.strip()])
            values = [float(x) for x in line1.strip().split()]
        data += values
    return data


def write_header_res_to_file(filename: tuple, res_filename: str):
    data = ret_header_from_plot(filename)
    tmp = None
    with open(res_filename, 'a') as file:
        file.write("{:.2f}".format(0.0))
        for val in data:
            if val != tmp:
                file.write("   {}   ".format(val))
            else:
                pass
            tmp = val
        file.write("\n")


def write_res_to_file(bup, filename: tuple, res_filename: str):
    data = ret_data_from_plot(filename)
    tmp = None
    with open(res_filename, 'a') as file:
        file.write("{:3} ".format(bup))
        for val in data:
            if val != tmp:
                file.write("{:.3e} ".format(val))
            else:
                pass
            tmp = val
        file.write("\n")


if __name__ == "__main__":
    # print(ret_data_from_plot('years._plot000.plt'))
    print(ret_header_from_plot(('years._plot000.plt',)))
    # write_header_res_to_file('years._plot000.plt', 'results-d.dat')
    # write_res_to_file(20.0, ('days_2._plot000.plt', 'days_2._plot000.plt'))
    # flip_rez("results-d.dat", 0)
