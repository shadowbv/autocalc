from jinja2 import Environment, FileSystemLoader
# import datetime, time


def ret_right_number(num):
    if num > 10:
        return num
    else:
        return "0{}".format(num)


def render_sas2h_template(burnup, template_name, power, step_for_bup, fuelenght, fuel_temp, mod_temp):
    file_loader = FileSystemLoader('templates')
    env = Environment(loader=file_loader)
    env.trim_blocks = True
    env.lstrip_blocks = True
    env.rstrip_blocks = True

    # template = env.get_template('SOU.inp')
    template = env.get_template('{}.inp'.format(template_name))
    bup_step = "{:.4f}".format(step_for_bup*1000/power)
    # print(bup_step)
    time = 55  # year
    step = step_for_bup  # year
    cool_time = 365 * time
    bups = [x + step for x in range(0, burnup, step)]
    number = len(bups) + 1
    print("sas2h bup points=", bups)
    # print(bups)
    output = template.render(number=ret_right_number(number), row=number, cool_time=cool_time, bup=bups, power=power,
                             bup_step=bup_step, fuelenght=fuelenght, fuel_temp=fuel_temp, mod_temp=mod_temp)
    # file = open("SOU.inp", "w")
    file = open("{}.inp".format(template_name), "w")
    file.write(output)
    # print(output)


def render_origens_template(template_name, saved_name, delays, number_point, time_type):
    file_loader = FileSystemLoader('templates')
    env = Environment(loader=file_loader)
    env.trim_blocks = True
    env.lstrip_blocks = True
    env.rstrip_blocks = True
    if time_type == 4:
        date = 'days'
    elif time_type == 5:
        date = 'year'

    # template = env.get_template('SOU.inp')
    template = env.get_template('{}.inp'.format(template_name))

    output = template.render(number_point=number_point, delays=delays, time_type=time_type, date=date)
    # file = open("SOU.inp", "w")
    file = open("{}.inp".format(saved_name), "w")
    file.write(output)
    # print(output)


if __name__ == "__main__":
    # render_sas2h_template(60, "438-440PK", 30, 5, 658, 800, 558)
    year_delays = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 15, 20, 25, 30, 40, 50, 55]
    time_type = 5  # year
    render_origens_template('origens', 'fuck', year_delays, len(year_delays), time_type)
# start = time.time()
# time.sleep(3)
# done = time.time()
# elapsed = done - start
# print(elapsed/60)
